FROM php:8.2-fpm-buster

ENV COMPOSER_ALLOW_SUPERUSER 1
ENV TZ ${TZ:-"Europe/Moscow"}
ENV PHP_OPCACHE_VALIDATE_TIMESTAMPS ${PHP_OPCACHE_VALIDATE_TIMESTAMPS:-0}
ENV PHP_OPCACHE_MAX_ACCELERATED_FILES ${PHP_OPCACHE_MAX_ACCELERATED_FILES:-10000}
ENV PHP_OPCACHE_MEMORY_CONSUMPTION ${PHP_OPCACHE_MEMORY_CONSUMPTION:-192}
ENV PHP_OPCACHE_MAX_WASTED_PERCENTAGE ${PHP_OPCACHE_MAX_WASTED_PERCENTAGE:-10}

RUN apt update
RUN apt install -y tzdata git mc wget curl yarn nodejs npm gnupg libfcgi0ldbl apt-transport-https apt-utils

COPY --from=mlocati/php-extension-installer /usr/bin/install-php-extensions /usr/local/bin/
RUN install-php-extensions gd intl zip mysqli pdo_mysql calendar exif imagick memcached pcntl sockets tidy xsl uuid gettext soap opcache bcmath

COPY --from=composer:2.5 /usr/bin/composer /usr/local/bin/composer
RUN echo "alias c='/usr/local/bin/composer'" >> /etc/bash.bashrc
RUN echo "alias ci='/usr/local/bin/composer install'" >> /etc/bash.bashrc

RUN curl -1sLf 'https://dl.cloudsmith.io/public/symfony/stable/setup.deb.sh' | bash
RUN apt install symfony-cli

COPY conf.d/php-extend.ini /usr/local/etc/php/conf.d/php-extend.ini
COPY conf.d/opcache.ini /usr/local/etc/php/conf.d/opcache.ini
 
COPY healthcheck/fpm-docker-healthcheck.conf /usr/local/etc/php-fpm.d/fpm-docker-healthcheck.conf
COPY healthcheck/docker-healthcheck /usr/local/bin/docker-healthcheck
RUN  chmod +x /usr/local/bin/docker-healthcheck
HEALTHCHECK --interval=10s --timeout=3s --retries=3 CMD ["docker-healthcheck"]

EXPOSE 9000

WORKDIR "/srv/app"

