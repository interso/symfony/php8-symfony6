SHELL=/bin/bash
BASE_DIR := $(shell dirname $(realpath $(lastword $(MAKEFILE_LIST))))

include $(BASE_DIR)/.env
-include $(BASE_DIR)/.env.local
export

help:
	@echo "Available command: build build-dev start-local start-remote connect dive security lint dockle"

build:
	docker build --pull --tag "$(IMAGE_NAME_LOCAL):latest" .

build-dev:
	docker build --pull --tag "$(IMAGE_NAME_LOCAL):latest" -f Dockerfile-dev  .

lint:
	docker run --rm -i  -v /var/run/docker.sock:/var/run/docker.sock hadolint/hadolint < Dockerfile

start-local:
	docker run -it --rm --name $(CONTAINER_NAME) $(IMAGE_NAME_LOCAL):latest

start-remote:
	docker run -it --rm --name $(CONTAINER_NAME) $(IMAGE_NAME_REMOTE):latest

connect:
	docker exec -it $(CONTAINER_NAME) /bin/bash

dive:
	docker run --rm -it -v /var/run/docker.sock:/var/run/docker.sock  wagoodman/dive:latest  $(IMAGE_NAME_LOCAL):latest

security:
	docker run --rm -v /var/run/docker.sock:/var/run/docker.sock aquasec/trivy image $(IMAGE_NAME_LOCAL):latest

dockle:
	docker run --rm -v /var/run/docker.sock:/var/run/docker.sock goodwithtech/dockle:latest $(IMAGE_NAME_LOCAL):latest

